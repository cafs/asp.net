﻿using ProjectMVC.Entidades;
using ProjectMVC.Negocio;
using ProjectMVC.Servicios.EntidadesContratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ProjectMVC.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "srvcCertificador" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select srvcCertificador.svc or srvcCertificador.svc.cs at the Solution Explorer and start debugging.
    public class srvcCertificador : IsrvcCertificador
    {
        public ExamenContract ObtenerExamen(short idExamen)
        {
            ExamenContract examenContrato = null;
            try
            {
                ExamenNegocio negocio = new ExamenNegocio();
                Examan examen = negocio.ObtenerExman(idExamen);

                if (examen != null)
                {
                    examenContrato = new ExamenContract()
                    {
                        Id = examen.Id,
                        Nombre = examen.Nombre,
                        Codigo = examen.Codigo,
                        Precio = examen.Precio
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return examenContrato;
        }

        public List<ExamenContract> ObtenerExamenes()
        {
            List<ExamenContract> examenesContrato = new List<ExamenContract>();
            try
            {
                ExamenNegocio negocio = new ExamenNegocio();
                List<Examan> examenes = negocio.ObtenerListaExmanes();

                foreach (var exam in examenes)
                {
                    examenesContrato.Add(new ExamenContract()
                    {
                        Id = exam.Id,
                        Nombre = exam.Nombre,
                        Codigo = exam.Codigo,
                        Precio = exam.Precio
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return examenesContrato;
        }
    }
}
