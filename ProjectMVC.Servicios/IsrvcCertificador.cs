﻿using ProjectMVC.Servicios.EntidadesContratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ProjectMVC.Servicios
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IsrvcCertificador" in both code and config file together.
    [ServiceContract]
    public interface IsrvcCertificador
    {
        [OperationContract]
        ExamenContract ObtenerExamen(short idExamen);
        [OperationContract]
        List<ExamenContract> ObtenerExamenes();
    }
}
