﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ProjectMVC.Servicios.EntidadesContratos
{
    [DataContract]
    public class ExamenContract
    {
        private short id;

        [DataMember]
        public short Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        [DataMember]
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string codigo;

        [DataMember]
        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private decimal precio;

        public decimal Precio
        {
            get { return precio; }
            set { precio = value; }
        }
    }
}