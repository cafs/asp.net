﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectMVC.Servicios.EntidadesContratos
{
    public class ExamenAjax
    {
        private short id;
        
        public short Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;
        
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string codigo;
        
        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private decimal precio;

        public decimal Precio
        {
            get { return precio; }
            set { precio = value; }
        }
    }
}