﻿using ProjectMVC.Entidades;
using ProjectMVC.Negocio;
using ProjectMVC.Servicios.EntidadesContratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace ProjectMVC.Servicios
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class srvcAjax
    {
        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";

        [OperationContract]
        public ExamenAjax ObtenerExamen(short idExamen)
        {
            ExamenAjax examenAjax = null;
            try
            {
                ExamenNegocio negocio = new ExamenNegocio();
                Examan examen = negocio.ObtenerExman(idExamen);

                if (examen != null)
                {
                    examenAjax = new ExamenAjax()
                    {
                        Id = examen.Id,
                        Nombre = examen.Nombre,
                        Codigo = examen.Codigo,
                        Precio = examen.Precio
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return examenAjax;
        }

        [OperationContract]
        public List<ExamenAjax> ObtenerExamenes()
        {
            List<ExamenAjax> examenAjax = new List<ExamenAjax>();
            try
            {
                ExamenNegocio negocio = new ExamenNegocio();
                List<Examan> examenes = negocio.ObtenerListaExmanes();

                foreach (var exam in examenes)
                {
                    examenAjax.Add(new ExamenAjax()
                    {
                        Id = exam.Id,
                        Nombre = exam.Nombre,
                        Codigo = exam.Codigo,
                        Precio = exam.Precio
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return examenAjax;
        }

        [OperationContract]
        public string ObtenerFotoInstructor(int idInstructor)
        {
            string fotoBase64 = string.Empty;
            try
            {
                InstructorNegocio negocio = new InstructorNegocio();
                ArchivoDTO fotoInstructor = negocio.ObtenerFotoInstructor(idInstructor);
                if (fotoInstructor != null && fotoInstructor.Contenido != null)
                {
                    fotoBase64 = Convert.ToBase64String(fotoInstructor.Contenido);
                    fotoBase64 = string.Concat("data:image/"
                                                            , fotoInstructor.Extension
                                                            , ";base64,"
                                                            , fotoBase64);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fotoBase64;
        }

        // Add more operations here and mark them with [OperationContract]
    }
}
