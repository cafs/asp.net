﻿using ProjectMVC.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Repositorio
{
    public class TecnologiaRepositorio
    {
        public TecnologiaRepositorio()
        {

        }

        public List<Tecnologia> ObtenerTecnologiasPorEstatus(bool activo)
        {
            try
            {
                List<Tecnologia> tecnologias = null;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    tecnologias = contexto.Tecnologias.Where(t => t.Activo == activo).ToList();
                }
                return tecnologias;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
