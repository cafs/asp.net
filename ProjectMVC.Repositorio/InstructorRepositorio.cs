﻿using ProjectMVC.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Repositorio
{
    public class InstructorRepositorio
    {
        public InstructorRepositorio()
        {

        }

        public Instructor ObtenerInstructor(int idInstructor)
        {
            try
            {
                Instructor instructor = null;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    contexto.Configuration.LazyLoadingEnabled = false;

                    instructor = contexto.Instructors.FirstOrDefault(i => i.Id == idInstructor);
                }
                return instructor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public bool ActualizarFotoInstructor(int idInstructor, ArchivoDTO archivo)
        {
            try
            {
                bool result = false;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    contexto.Configuration.LazyLoadingEnabled = false;
                    
                    // Buscamos al instructor a modificar por medio de extensiones de LINQ
                    Instructor instructor = contexto.Instructors
                        .FirstOrDefault(i => i.Id == idInstructor);

                    if (instructor != null)
                    {
                        // Hacemos la actualización de la foto y la extensión d ela foto
                        instructor.Foto = archivo.Contenido;
                        instructor.ExtensioFoto = archivo.Extension;

                        // Indicamos al contexto de Entity Framework que actualice el instructor
                        contexto.Instructors.Attach(instructor);
                        contexto.Entry(instructor).State = System.Data.Entity.EntityState.Modified;
                        contexto.SaveChanges();

                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
