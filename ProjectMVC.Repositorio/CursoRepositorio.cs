﻿using ProjectMVC.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Repositorio
{
    public class CursoRepositorio
    {
        public CursoRepositorio()
        {

        }

        public List<ConteoCursoDTO> ObtenerConteoPorCurso()
        {
            try
            {
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    contexto.Configuration.LazyLoadingEnabled = false;

                    var result = (from c in contexto.Cursoes
                                                   join cf in contexto.CursoOfertadoes
                                                   on c.Id equals cf.IdCurso
                                                   orderby c.Codigo
                                                   group c by new { c.Codigo, c.Nombre } into g
                                                   select new ConteoCursoDTO
                                                   {
                                                       Codigo = g.Key.Codigo,
                                                       Nombre = g.Key.Nombre,
                                                       CantidadCursosOfertados = g.Count()
                                                   }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
