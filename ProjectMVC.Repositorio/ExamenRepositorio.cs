﻿using ProjectMVC.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Repositorio
{
    public class ExamenRepositorio
    {
        public ExamenRepositorio()
        {

        }

        public List<Examan> ObtenerExamenes()
        {
            try
            {
                List<Examan> examenes = null;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    contexto.Configuration.LazyLoadingEnabled = false;
                    //examenes = contexto.Examen.OrderBy(e => e.Codigo).ThenBy(e => e.Nombre).ToList();
                    examenes = contexto.Examen.Include("Tecnologia").Include("Cursoes").Include("Examan1").OrderBy(e => e.Codigo).ThenBy(e => e.Nombre).ToList();
                }
                return examenes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertarExamen(Examan examen)
        {
            try
            {
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    contexto.Examen.Add(examen);
                    contexto.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarExamen(Examan examen)
        {
            try
            {
                bool result = false;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    // Obtener el objeto de examen con los datos actuales de BD
                    Examan examenAModificar = contexto.Examen.FirstOrDefault(x => x.Id == examen.Id);

                    if (examenAModificar != null)
                    {
                        // Vaciar los datos del usuario al objeto que apunta a base de datos
                        examenAModificar.Nombre = examen.Nombre;
                        examenAModificar.Codigo = examen.Codigo;
                        examenAModificar.Precio = examen.Precio;
                        examenAModificar.IdTecnologia = examen.IdTecnologia;
                        examenAModificar.Activo = examen.Activo;

                        // Inidcamos al contexto que se modificó el examen con id examen.id
                        contexto.Examen.Attach(examenAModificar);
                        contexto.Entry(examenAModificar).State = System.Data.Entity.EntityState.Modified;
                        contexto.SaveChanges();
                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarExamen(short idExamen)
        {
            try
            {
                bool result = false;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    // Obtener el objeto de examen con los datos actuales de BD
                    //Examan examenAEliminar = contexto.Examen.FirstOrDefault(x => x.Id == idExamen);
                    Examan examenAEliminar = (from e in contexto.Examen
                                              where e.Id == idExamen
                                              select e).FirstOrDefault();

                    if (examenAEliminar != null)
                    {
                        contexto.Examen.Remove(examenAEliminar);
                        contexto.SaveChanges();
                        result = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Examan ObtenerExamen(short idExamen)
        {
            try
            {
                Examan examen = null;
                using (CentroCertificadorEntities contexto = new CentroCertificadorEntities())
                {
                    contexto.Configuration.LazyLoadingEnabled = false;
                    // Obtener el objeto de examen con los datos actuales de BD
                    examen = contexto.Examen.Include("Cursoes").FirstOrDefault(x => x.Id == idExamen);
                }
                return examen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
