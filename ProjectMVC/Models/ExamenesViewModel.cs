﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class ExamenesViewModel
    {
        [Required]
        [Display(Name = "Identificador")]
        public short Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Nombre")]
        [StringLength(30, ErrorMessage = "El máximo de carácteres para el {0} es de {2}", MinimumLength = 5)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Código")]
        [StringLength(8, ErrorMessage = "El máximo de carácteres para el {0} es de {2}", MinimumLength = 5)]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Precio")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Precio { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Activo")]
        public bool Activo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Tecnología")]
        public short IdTecnologia { get; set; }
    }
}