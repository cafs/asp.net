﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models.Custom
{
    public class ListadoGenerico
    {
        public short Id { get; set; }
        public string Descripcion { get; set; }
    }
}