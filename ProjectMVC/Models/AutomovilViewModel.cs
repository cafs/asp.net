﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class AutomovilViewModel
    {
        [Required]
        [Display(Name = "Identificador")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Modelo")]
        [StringLength(50, ErrorMessage = "El másimo de carácteres para el {0} es de {2}", MinimumLength = 5)]
        public string Modelo { get; set; }

        [Required]
        [Display(Name = "Color")]
        [StringLength(15, ErrorMessage = "El másimo de carácteres para el {0} es de {2}", MinimumLength = 3)]
        public string Color { get; set; }

        [Required]
        [Display(Name = "No. de Placas")]
        [StringLength(10, ErrorMessage = "El másimo de carácteres para el {0} es de {2}", MinimumLength = 10)]
        public string Placas { get; set; }

        [Required]
        [Display(Name = "No. de Serie")]
        [StringLength(12, ErrorMessage = "El másimo de carácteres para el {0} es de {2}", MinimumLength = 12)]
        public string Serie { get; set; }

        [Required]
        [Display(Name = "No. de Puertas")]
        public int NumPuertas { get; set; }

        [Display(Name = "Cantidad de Ruedas")]
        public int NumRuedas { get; set; }

        [Required]
        [Display(Name = "Precio")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Precio { get; set; }
    }
}