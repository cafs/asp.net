﻿using ProjectMVC.Entidades;
using ProjectMVC.Models;
using ProjectMVC.Models.Custom;
using ProjectMVC.Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMVC.Controllers
{
    public class ExamenesController : Controller
    {
        // GET: Examenes
        public ActionResult Index()
        {
            refSrvcServidor.ExamenContract examen = null;
            List<refSrvcServidor.ExamenContract> examenes = null;
            using (refSrvcServidor.IsrvcCertificadorClient servicio 
                = new refSrvcServidor.IsrvcCertificadorClient())
            {
                examen = servicio.ObtenerExamen(1);
                examenes = servicio.ObtenerExamenes();
            }

            string nombre = examen.Nombre;

            ViewBag.ListadoTecnologias = ObtenerListaTecnologias();

            return View();
        }

        // GET: Listado de Examenes
        public ActionResult _ListadoExamenes()
        {
            ExamenNegocio negocio = new ExamenNegocio();
            List<Examan> examenes = negocio.ObtenerListaExmanes();

            return PartialView(examenes);
        }

        // GET: Crear Examen
        public ActionResult CrearExamen()
        {
            ViewBag.ListadoTecnologias = ObtenerListaTecnologias();

            return View();
        }

        // POST: Crear Examen
        [HttpPost]
        public ActionResult CrearExamen(ExamenesViewModel modelo)
        {
            if (ModelState.IsValid)
            {
                ExamenNegocio negocio = new ExamenNegocio();

                Examan examen = new Examan()
                {
                    Id = 0,
                    Codigo = modelo.Codigo,
                    Nombre = modelo.Nombre,
                    Precio = modelo.Precio,
                    Activo = modelo.Activo,
                    IdTecnologia = modelo.IdTecnologia
                };

                negocio.GuardarExamen(examen);

                return RedirectToAction("Index");
            }

            ViewBag.ListadoTecnologias = ObtenerListaTecnologias();

            return View(modelo);
        }

        // POST: Editar Examen
        [HttpPost]
        public ActionResult EditarExamen(ExamenesViewModel modelo)
        {
            try
            {
                ExamenNegocio negocio = new ExamenNegocio();

                Examan examen = new Examan()
                {
                    Id = modelo.Id,
                    Codigo = modelo.Codigo,
                    Nombre = modelo.Nombre,
                    Precio = modelo.Precio,
                    Activo = modelo.Activo,
                    IdTecnologia = modelo.IdTecnologia
                };

                negocio.GuardarExamen(examen);

                return Json(new { status = "OK", mensaje = "El examen se ha guardado correctamente" });
            }
            catch (Exception ex)
            {
                return Json(new { status = "ERROR", mensaje = "Ocurrió un error al intentar guardar el examen Detalle: " + ex.Message });
            }
        }

        // POST: Eliminar Examen
        [HttpPost]
        public ActionResult EliminarExamen(short idExamen)
        {
            try
            {

                ExamenNegocio negocio = new ExamenNegocio();

                negocio.EliminarExamen(idExamen);

                return Json(new { status = "OK", mensaje = "El examen se ha eliminado correctamente" });
            }
            catch (Exception ex)
            {
                return Json(new { status = "ERROR", mensaje = "Ocurrió un error al intentar eliminar el examen. Detalle: " + ex.Message });
            }
        }

        private List<ListadoGenerico> ObtenerListaTecnologias()
        {
            try
            {
                TecnologiaNegocio negocio = new TecnologiaNegocio();
                List<Tecnologia> tecnologias = negocio.ObtenerTecnologiasActivas();

                List<ListadoGenerico> listado = new List<ListadoGenerico>();
                foreach (var tec in tecnologias)
                {
                    listado.Add(new ListadoGenerico()
                    {
                        Id = tec.Id,
                        Descripcion = tec.Descripcion

                    });
                }

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}