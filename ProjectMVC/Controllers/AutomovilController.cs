﻿using ProjectMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMVC.Controllers
{
    public class AutomovilController : Controller
    {
        // GET: Automovil
        public ActionResult Index()
        {
            ViewBag.MensajeListado = "Aquí se muestra el listado de todos los automóviles registrados.";
            return View();
        }

        // GET: Automovil/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }

        // GET: Automovil/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Automovil/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Color,Precio")] AutomovilViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add insert logic here

                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Automovil/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Automovil/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Automovil/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Automovil/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
