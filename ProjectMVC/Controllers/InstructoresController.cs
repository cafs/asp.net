﻿using ProjectMVC.Entidades;
using ProjectMVC.Negocio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMVC.Controllers
{
    public class InstructoresController : Controller
    {
        // GET: Instructores
        public ActionResult Index(int id)
        {
            InstructorNegocio negocio = new InstructorNegocio();
            ArchivoDTO archivo = negocio.ObtenerFotoInstructor(id);

            ViewBag.IdInstructor = id;

            return View(archivo);
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult IndexUpload(int id)
        {
            InstructorNegocio negocio = new InstructorNegocio();

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    Stream strm = file.InputStream;
                    string[] cadenasExtension = file.ContentType.Split('/');

                    ArchivoDTO archivo = new ArchivoDTO()
                    {
                        Contenido = ObtenerBytes(strm),
                        Extension = cadenasExtension[cadenasExtension.Length-1]
                    };
                    negocio.ActualizarFotoInstructor(id, archivo);
                }
            }

            ArchivoDTO archivoBD = negocio.ObtenerFotoInstructor(id);
            return View(archivoBD);
        }

        [HttpGet]
        public FileResult DescargarFoto(int id)
        {
            InstructorNegocio negocio = new InstructorNegocio();
            ArchivoDTO archivo = negocio.ObtenerFotoInstructor(id);

            if (archivo != null && archivo.Contenido != null)
            {
                string nombreArchivo = string.Concat("foto.",archivo.Extension);
                string contentType = MimeMapping.GetMimeMapping(nombreArchivo);

                return File(archivo.Contenido, contentType, nombreArchivo);
            }
            return null;
        }

        private byte[] ObtenerBytes(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}