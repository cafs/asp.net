﻿using ProjectMVC.Entidades;
using ProjectMVC.Negocio;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMVC.Controllers
{
    public class CursosController : Controller
    {
        // GET: Cursos
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult ReporteConteoCursos()
        {
            return View();
        }

        public ActionResult _ListadoCursosConteo()
        {
            CursoNegocio negocio = new CursoNegocio();
            List<ConteoCursoDTO> conteo = negocio.ObtenerConteoPorCursoOfertado();

            return PartialView(conteo);
        }

        public ActionResult ExportarListadoCursosConteo()
        {
            var vistaPDF = new ViewAsPdf();
            vistaPDF.ViewName = "ReporteConteoCursos";
            vistaPDF.CustomSwitches = "--footer-left \"" + DateTime.Now.ToString("dd/MM/yyyy") + "\""
                                    + " --footer-right \"Página [page] de [toPage]\""
                                    + " --footer-font-size \"9\" --footer-spacing 3";
            vistaPDF.Model = null;
            vistaPDF.PageSize = Size.A4;
            vistaPDF.PageOrientation = Orientation.Landscape;

            var pdfBytes = vistaPDF.BuildPdf(ControllerContext);

            //string nombreArchivo = HttpUtility.UrlEncode("ReporteConteo.pdf", System.Text.Encoding.UTF8);

            ArchivoDTO archivo = new ArchivoDTO()
            {
                Contenido = pdfBytes,
                Extension = ".pdf"
            };

            Session["rptConeto"] = archivo;
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public void DescargaArchivo()
        {
            if (Session["rptConeto"] != null)
            {
                ArchivoDTO archivo = Session["rptConeto"] as ArchivoDTO;
                Session.Remove("rptConeto");

                // Usar Response para realizar la descarga
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition","attachment; filename=Reporte."+archivo.Extension);
                Response.BinaryWrite(archivo.Contenido);
                Response.Flush();
                Response.End();
            }
        }
    }
}