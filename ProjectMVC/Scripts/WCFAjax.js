﻿function errorGenerico(container, jqXHR, exception) {
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'No está conectado, favor de verificar su conexión.';
    }
    else if (jqXHR.status == 404) {
        msg = 'Página no encontrada [404].';
    }
    else if (jqXHR.status == 500) {
        msg = 'Error en el servidor [500].';
    }
    else if (exception === 'parseerror') {
        msg = 'El parseo del JSON es erróneo.';
    }
    else if (exception === 'timeout') {
        msg = 'Error por tiempo de espera.';
    }
    else if (exception === 'abort') {
        msg = 'La petición Ajax fue abortada.';
    }
    else {
        msg = 'Error no conocido. ' + jqXHR.responseText;
    }
    $('#' + container).html("<span class'text-danger'>" + msg + "</span>");
}

function numeroConCommas(x) {
    var parts = (parseFloat(x).toFixed(2)).toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function pintarListado(contenedor, data) {
    console.log(data);
    var miData = data.d;

    var miDiv = $("#" + contenedor);

    miDiv.empty();

    if (miData != undefined && miData != null && miData.length > 0) {
        var selectList = document.createElement('select');
        selectList.id = "selectObjetos";
        miDiv.append(selectList);

        for (var i = 0; i < miData.length; i++) {
            var option = document.createElement('option')
            option.value = miData[i].Id;
            option.text = miData[i].Nombre;
            selectList.append(option);
        }
        selectList.classList.add('form-control');
    }
    else {
        $("#" + contenedor).html("El listado está vacío");
    }
}

function pintarObjeto(contenedor, data) {
    console.log(data);
    var miData = data.d;

    var miDiv = $("#" + contenedor);

    miDiv.empty();

    if (miData != undefined && miData != null) {
        // Crear Form Horizontal
        var divFormHorizontal = document.createElement('div');
        divFormHorizontal.classList.add('form-horizontal');

        // ------------------------------------------
        // Crear Form Group 1
        var divFormGroup = document.createElement('div');
        divFormGroup.classList.add('form-group');
        var label = document.createElement('label');
        label.id = 'lblCodigo';
        label.htmlFor = 'txtCodigo';
        label.classList.add('control-label');
        label.textContent = 'Código: ';
        var text = document.createElement('input');
        text.id = "txtCodigo";
        text.type = "text";
        text.placeholder = "Código del examen";
        text.value = miData.Codigo;
        text.classList.add("form-control");
        // Agregar elementos de un form-group 1
        divFormGroup.append(label);
        divFormGroup.append(text);
        divFormHorizontal.append(divFormGroup);
        // ------------------------------------------
        // Crear Form Group 2
        divFormGroup = document.createElement('div');
        divFormGroup.classList.add('form-group');

        label = document.createElement('label');
        label.id = 'lblNombre';
        label.htmlFor = 'txtNombre';
        label.classList.add('control-label');
        label.textContent = 'Nombre: ';
        text = document.createElement('input');
        text.id = "txtNombre";
        text.type = "text";
        text.placeholder = "Nombre del examen";
        text.value = miData.Nombre;
        text.classList.add("form-control");
        // Agregar elementos de un form-group 2
        divFormGroup.append(label);
        divFormGroup.append(text);
        divFormHorizontal.append(divFormGroup);
        // ------------------------------------------
        // Crear Form Group 3
        divFormGroup = document.createElement('div');
        divFormGroup.classList.add('form-group');

        label = document.createElement('label');
        label.id = 'lblPrecio';
        label.htmlFor = 'txtPrecio';
        label.classList.add('control-label');
        label.textContent = 'Precio: ';
        text = document.createElement('input');
        text.id = "txtPrecio";
        text.type = "text";
        text.placeholder = "Precio del examen";
        text.value = numeroConCommas(miData.Precio);
        text.classList.add("form-control");
        // Agregar elementos de un form-group -3
        divFormGroup.append(label);
        divFormGroup.append(text);
        divFormHorizontal.append(divFormGroup);
        // ------------------------------------------

        // Agregar al contenedor (div principal) el form-horizontal
        miDiv.append(divFormHorizontal);
        miDiv.append(document.createElement('br'));
    }
    else {
        $("#" + contenedor).html("<span class='text-danger'>No se pudo obtener el examen</span>");
    }
}

function ajaxObjeto(contenedor, urlWS, paramWS) {
    $.ajax({
        type: 'POST',
        url: urlWS,
        data: JSON.stringify(paramWS),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            pintarObjeto(contenedor, data);
        },
        error: function (jqXHR, textStatus) {
            errorGenerico(contenedor, jqXHR, textStatus);
        }
    });
}

function ajaxListado(contenedor, urlWS, paramWS)
{
    $.ajax({
        type: 'POST',
        url: urlWS,
        data: JSON.stringify(paramWS),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            pintarListado(contenedor, data);
        },
        error: function (jqXHR, textStatus) {
            errorGenerico(contenedor,jqXHR, textStatus);
        }
    });
}

function ajaxImg(img, contenedor, urlWS, paramWS) {
    $.ajax({
        type: 'POST',
        url: urlWS,
        data: JSON.stringify(paramWS),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            var miData = data.d;

            var imageElement = document.createElement("img");
            imageElement.setAttribute("src", miData);

            $("#divImgAjaxWCF").empty();
            $("#divImgAjaxWCF").append(imageElement);

        },
        error: function (jqXHR, textStatus) {
            errorGenerico(contenedor, jqXHR, textStatus);
        }
    });
}

