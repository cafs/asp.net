﻿using ProjectMVC.Entidades;
using ProjectMVC.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Negocio
{
    public class ExamenNegocio
    {
        public ExamenNegocio()
        {

        }

        public Examan ObtenerExman(short idExamen)
        {
            try
            {
                ExamenRepositorio repositorio = new ExamenRepositorio();
                return repositorio.ObtenerExamen(idExamen);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Examan> ObtenerListaExmanes()
        {
            try
            {
                ExamenRepositorio repositorio = new ExamenRepositorio();
                return repositorio.ObtenerExamenes();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GuardarExamen(Examan examen)
        {
            try
            {
                ExamenRepositorio repositorio = new ExamenRepositorio();
                if (examen.Id <= 0)
                {
                    repositorio.InsertarExamen(examen);
                }
                else
                {
                    repositorio.ActualizarExamen(examen);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarExamen(short idExamen)
        {
            try
            {
                bool result = false;

                ExamenRepositorio repositorio = new ExamenRepositorio();
                Examan examen = repositorio.ObtenerExamen(idExamen);

                if (examen != null && examen.Cursoes.Count() <= 0 && !examen.Activo)
                {
                    result = repositorio.EliminarExamen(idExamen);
                }
                else
                {
                    throw new Exception("No se puede eliminar el examen porque tiene cursos asociados o se encuentra activo");
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
