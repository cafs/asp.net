﻿using ProjectMVC.Entidades;
using ProjectMVC.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Negocio
{
    public class TecnologiaNegocio
    {
        public TecnologiaNegocio()
        {

        }

        public List<Tecnologia> ObtenerTecnologiasActivas()
        {
            try
            {
                TecnologiaRepositorio repositorio = new TecnologiaRepositorio();

                return repositorio.ObtenerTecnologiasPorEstatus(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
