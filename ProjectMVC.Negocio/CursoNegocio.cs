﻿using ProjectMVC.Entidades;
using ProjectMVC.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Negocio
{
    public class CursoNegocio
    {
        public CursoNegocio()
        {

        }

        public List<ConteoCursoDTO> ObtenerConteoPorCursoOfertado()
        {
            try
            {
                CursoRepositorio repositorio = new CursoRepositorio();
                return repositorio.ObtenerConteoPorCurso();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
