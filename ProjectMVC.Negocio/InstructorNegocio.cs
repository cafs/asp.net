﻿using ProjectMVC.Entidades;
using ProjectMVC.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Negocio
{
    public class InstructorNegocio
    {
        public InstructorNegocio()
        {

        }

        public ArchivoDTO ObtenerFotoInstructor(int idInstructor)
        {
            try
            {
                // Obtenemos el objeto de instructor con los datos del instructor con id = idInstructor
                InstructorRepositorio repositorio = new InstructorRepositorio();
                Instructor instructor = repositorio.ObtenerInstructor(idInstructor);

                // Creamos un objeto de tipo ArchivoDTO para poder transportar los datos a la
                // capa de presentación
                ArchivoDTO archivo = new ArchivoDTO();
                archivo.Contenido = new byte[0];
                archivo.Extension = "jpg";

                if (instructor != null)
                {
                    archivo.Contenido = instructor.Foto;
                    archivo.Extension = instructor.ExtensioFoto;
                }

                return archivo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarFotoInstructor(int idInstructor, ArchivoDTO archivo)
        {
            try
            {
                int tamaño = archivo.Contenido.Length;

                if (tamaño < 2097152) // 2MB
                {
                    // Actualizamos por medio del repositorio la foto del instructor
                    InstructorRepositorio repositorio = new InstructorRepositorio();
                    return repositorio.ActualizarFotoInstructor(idInstructor, archivo);
                }
                throw new Exception("No se puede registrar una foto mayor a 2 MB");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
