﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Entidades
{
    public class ConteoCursoDTO
    {
        private string codigo;

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        private int cantidadCursosOfertados;

        public int CantidadCursosOfertados
        {
            get { return cantidadCursosOfertados; }
            set { cantidadCursosOfertados = value; }
        }

    }
}
