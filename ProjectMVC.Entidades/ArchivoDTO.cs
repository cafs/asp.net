﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectMVC.Entidades
{
    public class ArchivoDTO
    {
        private string extension;

        public string Extension
        {
            get { return extension; }
            set { extension = value; }
        }

        private byte[] contenido;

        public byte[] Contenido
        {
            get { return contenido; }
            set { contenido = value; }
        }

    }
}
